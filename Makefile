HPP_DIR = include
CPP_DIR = source
BIN_DIR = build
BIN_NAME = mastermind

build: createOutputDir
	g++ --std=c++17 -Wall $(CPP_DIR)/*.cpp -I$(CURDIR)/include -o $(BIN_DIR)/$(BIN_NAME)

createOutputDir:
	mkdir -p ./$(BIN_DIR)

run:
	chmod +x ./$(BIN_DIR)/${BIN_NAME} && ./$(BIN_DIR)/${BIN_NAME}