#pragma once

#include "boardDrawer.h"
#include "cli.h"
#include "peg.h"

#include <atomic>
#include <condition_variable>
#include <functional>
#include <thread>

class BoardThreadedCliDrawer : public BoardDrawer
{
 public:
   BoardThreadedCliDrawer(Cli &cli);
   ~BoardThreadedCliDrawer();
   void draw(const BoardData &boardData) override;
   void blankOutCode();
   void showCode();

 private:
   void drawCached();
   void printBlankCode(const Peg::Row &row) const;
   void printCode(const Peg::Row &row) const;
   char symbolForColor(Peg::Color color) const;

   Cli &m_cli;
   std::function<void(const Peg::Row &row)> m_codePrintStrategy;
   std::mutex m_mutex;
   std::condition_variable m_cv;
   std::atomic<bool> m_working;
   std::thread m_thread;
   std::unique_ptr<BoardData> m_cached;
};
