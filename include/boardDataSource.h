#pragma once

#include "cli.h"
#include "peg.h"

class BoardDataSource
{
 public:
   virtual ~BoardDataSource() = default;
   virtual Peg::Row getRow(bool allowDuplicates) const = 0;
};

class BoardHumanDataSource : public BoardDataSource
{
 public:
   BoardHumanDataSource(Cli &cli);
   Peg::Row getRow(bool allowDuplicates) const override;

 private:
   Cli &m_cli;
};

class BoardCpuDataSource : public BoardDataSource
{
 public:
   BoardCpuDataSource() = default;
   Peg::Row getRow(bool allowDuplicates) const override;

 private:
   Peg::Color getRandomColor() const;
};