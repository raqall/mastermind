#pragma once

#include "peg.h"
#include "player.h"
#include "settings.h"

#include <string>
#include <vector>

struct BoardData
{
   BoardData(const Settings &settings, Player &playerOne, Player &playerTwo);
   void clear();

   const Settings settings;

   Peg::Row code;
   std::vector<Peg::Row> hints;
   std::vector<Peg::Row> rows;
   std::string message;

   Player &playerOne;
   Player &playerTwo;
   unsigned currentRound;
};