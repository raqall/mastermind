#pragma once

#include <array>
#include <set>
#include <string>

class Peg
{
 public:
   using Slot = char;

   enum class Color
   {
      _blank = 0,
      red = 1,
      green = 2,
      blue = 3,
      yellow = 4,
      orange = 5,
      purple = 6,
      white = 7,
      black = 8
   };

   using Row = std::array<Peg::Color, 4>;

   static std::pair<unsigned /* exact */, unsigned /* colorOnly */> countMaching(const Peg::Row &a, const Peg::Row &b);
   static unsigned colorToUint(Peg::Color color);
   static Peg::Color uintToColor(unsigned index);
   static std::string bigPegIndexColorStrInfo();
   static Peg::Row rowFromSet(const std::set<unsigned> &set);
};