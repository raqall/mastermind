#pragma once

#include "format.h"

#include <string>
#include <string_view>

class Cli
{
 public:
   void print();
   void print(std::string_view message, bool nLine = true);
   std::string getText(std::string_view message);
   bool getBool(std::string_view message);
   unsigned getUnsigned(std::string_view message);
};