#pragma once

#include "boardDrawer.h"
#include "cli.h"
#include "peg.h"

#include <functional>

class BoardCliDrawer : public BoardDrawer
{
 public:
   BoardCliDrawer(Cli &cli);
   void draw(const BoardData &boardData) override;
   void blankOutCode();
   void showCode();

 private:
   void printBlankCode(const Peg::Row &row) const;
   void printCode(const Peg::Row &row) const;
   char symbolForColor(Peg::Color color) const;

   Cli &m_cli;
   std::function<void(const Peg::Row &row)> m_codePrintStrategy;
};
