#pragma once

#include <sstream>
#include <string>

class Format
{
   template <typename T>
   static void internalFormat(std::ostream &o, T t)
   {
      o << t;
   }

   template <typename T, typename... Args>
   static void internalFormat(std::ostream &o, T t, Args... args)
   {
      internalFormat(o, t);
      internalFormat(o, args...);
   }

 public:
   template <typename... Args>
   static std::string format(Args... args)
   {
      std::ostringstream oss{};
      internalFormat(oss, args...);
      return oss.str();
   }
};