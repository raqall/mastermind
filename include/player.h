#pragma once

#include "boardDataSource.h"
#include "peg.h"

#include <memory>
#include <string>
#include <string_view>

class Player
{
 public:
   Player(std::string_view name, std::shared_ptr<BoardDataSource> dataSrc);
   Peg::Row getRow(bool allowDuplicates);
   std::string_view getName() const;
   unsigned getScore() const;
   void awardPoint();

 private:
   const std::string m_name;
   unsigned m_score;
   const std::shared_ptr<BoardDataSource> m_dataSrc;
};
