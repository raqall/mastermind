#pragma once

#include "cli.h"
#include "settings.h"

#include <string>

class SettingsBuilder;

class Settings
{
   friend class SettingsBuilder;

 public:
   std::string playerOneName;
   std::string playerTwoName;
   bool playerOneCpu;
   bool playerTwoCpu;
   bool allowDuplicates;
   unsigned rounds;
   unsigned rows;
   unsigned rowSlots;
   bool codeVisible;

 private:
   Settings() = default;
};

class SettingsBuilder
{
 public:
   static Settings get(Cli &cli);

 private:
   SettingsBuilder(Cli &cli, Settings &settings);
   bool getAllowDuplicates() const;
   bool getRounds() const;
   bool getRows() const;
   bool getPlayerOneCpu() const;
   bool getPlayerTwoCpu() const;
   bool getCodeVisible() const;

   Cli &m_cli;
   Settings &m_settings;
};