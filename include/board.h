#pragma once

#include "boardData.h"
#include "boardDataSource.h"
#include "player.h"
#include "settings.h"

#include <functional>

class Board
{
 public:
   Board(const Settings &settings, Player &playerOne, Player &playerTwo);
   void setViewUpdatedListener(const std::function<void(const BoardData &)> &fun);
   void play();

 private:
   void viewUpdated() const;
   bool validateRow(const Peg::Row &row) const;

   BoardData m_boardData;
   std::function<void(const BoardData &)> m_onBoardUpdateHdlr;
};