#pragma once

#include "boardData.h"

class BoardDrawer
{
 public:
   virtual void draw(const BoardData &) = 0;
};