#include "boardThreadedCliDrawer.h"

#include <iostream> // TODO: remove

BoardThreadedCliDrawer::BoardThreadedCliDrawer(Cli &cli)
  : m_cli{cli}
  , m_codePrintStrategy{[this](const Peg::Row &row) { printCode(row); }}
  , m_working{true}
  , m_thread{[this]() { drawCached(); }}
{
}

BoardThreadedCliDrawer::~BoardThreadedCliDrawer()
{
   m_working = false;
   m_cv.notify_all();
   m_thread.join();
}

void BoardThreadedCliDrawer::drawCached()
{
   while (m_working)
   {
      std::unique_lock lock{m_mutex};
      m_cv.wait(lock);

      if (!m_cached)
      {
         continue;
      }

      m_cli.print();
      m_cli.print(Format::format("Round ", m_cached->currentRound, "/", m_cached->settings.rounds));
      m_cli.print(Format::format(m_cached->playerOne.getName(), ": ", m_cached->playerOne.getScore(), " points"));
      m_cli.print(Format::format(m_cached->playerTwo.getName(), ": ", m_cached->playerTwo.getScore(), " points"));

      m_cli.print();
      m_cli.print(" M A S T E R M I N D");
      m_cli.print("░░░░░░░░░░░░1░2░3░4░░");

      for (auto r{0u}; r < m_cached->rows.size(); r++)
      {
         m_cli.print("░ ", false);

         for (const auto &peg : m_cached->hints.at(r))
         {
            m_cli.print(Format::format(symbolForColor(peg), " "), false);
         }

         m_cli.print("  ", false);

         for (const auto &peg : m_cached->rows.at(r))
         {
            m_cli.print(Format::format(symbolForColor(peg), " "), false);
         }

         m_cli.print("░");
      }

      m_cli.print("░───────────────────░");
      m_cli.print("░           ", false);

      m_codePrintStrategy(m_cached->code);

      m_cli.print("░");
      m_cli.print("░░░░░░░░░░░░░░░░░░░░░");
      m_cli.print();

      if (!m_cached->message.empty())
      {
         m_cli.print(m_cached->message);
         m_cli.print();
      }
   }
}

void BoardThreadedCliDrawer::draw(const BoardData &boardData)
{
   std::lock_guard lock{m_mutex};
   m_cached = std::make_unique<BoardData>(boardData);
   m_cv.notify_all();
}

void BoardThreadedCliDrawer::blankOutCode()
{
   m_codePrintStrategy = [this](const Peg::Row &row) { printBlankCode(row); };
}

void BoardThreadedCliDrawer::showCode()
{
   m_codePrintStrategy = [this](const Peg::Row &row) { printCode(row); };
}

char BoardThreadedCliDrawer::symbolForColor(Peg::Color color) const
{
   switch (color)
   {
   case Peg::Color::red:
      return 'R';
   case Peg::Color::green:
      return 'G';
   case Peg::Color::blue:
      return 'B';
   case Peg::Color::yellow:
      return 'Y';
   case Peg::Color::orange:
      return 'O';
   case Peg::Color::purple:
      return 'P';
   case Peg::Color::white:
      return 'W';
   case Peg::Color::black:
      return 'B';
   default:
      return '-';
   }
}

void BoardThreadedCliDrawer::printBlankCode(const Peg::Row &row) const
{
   for (const auto &peg : row)
   {
      m_cli.print(peg == Peg::Color::_blank ? Format::format(symbolForColor(peg), " ") : "* ", false);
   }
}

void BoardThreadedCliDrawer::printCode(const Peg::Row &row) const
{
   for (const auto &peg : row)
   {
      m_cli.print(Format::format(symbolForColor(peg), " "), false);
   }
}
