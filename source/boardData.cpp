#include "boardData.h"

BoardData::BoardData(const Settings &settings, Player &playerOne, Player &playerTwo)
  : settings{settings}
  , code{}
  , message{}
  , playerOne{playerOne}
  , playerTwo{playerTwo}
  , currentRound{0u}
{
   for (auto r{0u}; r < settings.rows; r++)
   {
      hints.emplace_back(Peg::Row{});
      rows.emplace_back(Peg::Row{});
   }
}

void BoardData::clear()
{
   code.fill(Peg::Color::_blank);

   for (auto &hint : hints)
   {
      hint.fill(Peg::Color::_blank);
   }
   for (auto &row : rows)
   {
      row.fill(Peg::Color::_blank);
   }
}