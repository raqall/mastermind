#include "cli.h"

#include <iostream>

void Cli::print()
{
   std::cout << std::endl;
}

void Cli::print(std::string_view message, bool nLine)
{
   std::cout << message;
   if (nLine)
   {
      std::cout << std::endl;
   }
}

std::string Cli::getText(std::string_view message)
{
   std::cout << message;
   std::string input{};
   std::getline(std::cin, input);
   return input;
}

bool Cli::getBool(std::string_view message)
{
   const auto input{getText(message)};
   if (input == "yes" || input == "y" || input == "YES" || input == "Y" || input == "1")
   {
      return true;
   }
   else if (input == "no" || input == "n" || input == "NO" || input == "N" || input == "0")
   {
      return false;
   }

   throw std::runtime_error{Format::format("Invalid input: cannot convert '", input, "' into boolean")};
}

unsigned Cli::getUnsigned(std::string_view message)
{
   const auto input{getText(message)};
   try
   {
      const auto floating{std::stod(input)};
      if (floating < 0)
      {
         throw std::runtime_error{""};
      }
      return unsigned(floating);
   }
   catch (...)
   {
      throw std::runtime_error{Format::format("Invalid input: cannot convert '", input, "' into unsigned int")};
   }
}
