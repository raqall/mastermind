#include "boardCliDrawer.h"

BoardCliDrawer::BoardCliDrawer(Cli &cli)
  : m_cli{cli}
  , m_codePrintStrategy{[this](const Peg::Row &row) { printCode(row); }}
{
}

void BoardCliDrawer::draw(const BoardData &boardData)
{
   m_cli.print();
   m_cli.print(Format::format("Round ", boardData.currentRound, "/", boardData.settings.rounds));
   m_cli.print(Format::format(boardData.playerOne.getName(), ": ", boardData.playerOne.getScore(), " points"));
   m_cli.print(Format::format(boardData.playerTwo.getName(), ": ", boardData.playerTwo.getScore(), " points"));

   m_cli.print();
   m_cli.print(" M A S T E R M I N D");
   m_cli.print("░░░░░░░░░░░░1░2░3░4░░");

   for (auto r{0u}; r < boardData.rows.size(); r++)
   {
      m_cli.print("░ ", false);

      for (const auto &peg : boardData.hints.at(r))
      {
         m_cli.print(Format::format(symbolForColor(peg), " "), false);
      }

      m_cli.print("  ", false);

      for (const auto &peg : boardData.rows.at(r))
      {
         m_cli.print(Format::format(symbolForColor(peg), " "), false);
      }

      m_cli.print("░");
   }

   m_cli.print("░───────────────────░");
   m_cli.print("░           ", false);

   m_codePrintStrategy(boardData.code);

   m_cli.print("░");
   m_cli.print("░░░░░░░░░░░░░░░░░░░░░");
   m_cli.print();

   if (!boardData.message.empty())
   {
      m_cli.print(boardData.message);
      m_cli.print();
   }
}

void BoardCliDrawer::blankOutCode()
{
   m_codePrintStrategy = [this](const Peg::Row &row) { printBlankCode(row); };
}

void BoardCliDrawer::showCode()
{
   m_codePrintStrategy = [this](const Peg::Row &row) { printCode(row); };
}

char BoardCliDrawer::symbolForColor(Peg::Color color) const
{
   switch (color)
   {
   case Peg::Color::red:
      return 'R';
   case Peg::Color::green:
      return 'G';
   case Peg::Color::blue:
      return 'B';
   case Peg::Color::yellow:
      return 'Y';
   case Peg::Color::orange:
      return 'O';
   case Peg::Color::purple:
      return 'P';
   case Peg::Color::white:
      return 'W';
   case Peg::Color::black:
      return 'B';
   default:
      return '-';
   }
}

void BoardCliDrawer::printBlankCode(const Peg::Row &row) const
{
   for (const auto &peg : row)
   {
      m_cli.print(peg == Peg::Color::_blank ? Format::format(symbolForColor(peg), " ") : "* ", false);
   }
}

void BoardCliDrawer::printCode(const Peg::Row &row) const
{
   for (const auto &peg : row)
   {
      m_cli.print(Format::format(symbolForColor(peg), " "), false);
   }
}
