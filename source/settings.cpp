#include "format.h"
#include "settings.h"

Settings SettingsBuilder::get(Cli &cli)
{
   Settings settings{};
   SettingsBuilder builder{cli, settings};

   settings.playerOneName = cli.getText("PLayer one name: ");
   settings.playerTwoName = cli.getText("PLayer two name: ");
   // clang-format off
   while (!builder.getPlayerOneCpu()){}
   while (!builder.getPlayerTwoCpu()){}
   while (!builder.getAllowDuplicates()){}
   while (!builder.getRounds()){}
   while (!builder.getRows()){}
   while (!builder.getCodeVisible()){}
   // clang-format on
   settings.rowSlots = 4;

   return settings;
}

SettingsBuilder::SettingsBuilder(Cli &cli, Settings &settings)
  : m_cli{cli}
  , m_settings{settings}
{
}

bool SettingsBuilder::getPlayerOneCpu() const
{
   try
   {
      m_settings.playerOneCpu = m_cli.getBool("Shall player one be controlled by CPU? (y/n): ");
      return true;
   }
   catch (const std::exception &e)
   {
      m_cli.print(e.what());
   }

   return false;
}

bool SettingsBuilder::getPlayerTwoCpu() const
{
   try
   {
      m_settings.playerTwoCpu = m_cli.getBool("Shall player two be controlled by CPU? (y/n): ");
      return true;
   }
   catch (const std::exception &e)
   {
      m_cli.print(e.what());
   }

   return false;
}

bool SettingsBuilder::getCodeVisible() const
{
   try
   {
      m_settings.codeVisible = m_cli.getBool("Shall the code be visible? (y/n): ");
      return true;
   }
   catch (const std::exception &e)
   {
      m_cli.print(e.what());
   }

   return false;
}

bool SettingsBuilder::getAllowDuplicates() const
{
   try
   {
      m_settings.allowDuplicates = m_cli.getBool("Are pegs of the same collor allowed? (y/n): ");
      return true;
   }
   catch (const std::exception &e)
   {
      m_cli.print(e.what());
   }

   return false;
}

bool SettingsBuilder::getRows() const
{
   try
   {
      auto rows{m_cli.getUnsigned("Attempts in a round (row count): ")};
      if (rows < 8 || rows > 12)
      {
         m_cli.print(Format::format("Row count must be between 8 and 12 inclusive"));
         return false;
      }

      m_settings.rows = rows;
      return true;
   }
   catch (const std::exception &e)
   {
      m_cli.print(e.what());
   }

   return false;
}

bool SettingsBuilder::getRounds() const
{
   try
   {
      auto rounds{m_cli.getUnsigned("How many rounds you would like to play: ")};
      if (rounds == 0 || rounds % 2)
      {
         m_cli.print(Format::format("You may only play even number of rounds"));
         return false;
      }

      m_settings.rounds = rounds;
      return true;
   }
   catch (const std::exception &e)
   {
      m_cli.print(e.what());
   }

   return false;
}