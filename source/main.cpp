#include "board.h"
#include "boardThreadedCliDrawer.h"
#include "boardDataSource.h"
#include "cli.h"
#include "settings.h"

#include <iostream>
#include <string>

int main()
{
   Cli cli{};
   BoardThreadedCliDrawer bdrw{cli};

   const auto settings{SettingsBuilder::get(cli)};

   const auto getDataSource{[&](bool humanControll) {
      std::shared_ptr<BoardDataSource> dataSource{};
      if (humanControll)
      {
         dataSource = std::make_shared<BoardCpuDataSource>();
      }
      else
      {
         dataSource = std::make_shared<BoardHumanDataSource>(cli);
      }
      return dataSource;
   }};

   Player playerOne{settings.playerOneName, getDataSource(settings.playerOneCpu)};
   Player playerTwo{settings.playerTwoName, getDataSource(settings.playerTwoCpu)};

   if (!settings.codeVisible)
   {
      bdrw.blankOutCode();
   }

   Board board{settings, playerOne, playerTwo};
   board.setViewUpdatedListener([&](const BoardData &boardData) { bdrw.draw(boardData); });

   board.play();

   cli.print();
   cli.print("Game over!");
   if (playerOne.getScore() == playerTwo.getScore())
   {
      cli.print(Format::format("Tie! Both ", playerOne.getName(), " and ", playerTwo.getName(), " scored ",
                               playerOne.getScore(), " points."));
   }
   else
   {
      const auto &winner{(playerOne.getScore() > playerTwo.getScore()) ? playerOne : playerTwo};
      const auto &loser{(playerOne.getScore() < playerTwo.getScore()) ? playerOne : playerTwo};

      cli.print(Format::format("Player ", winner.getName(), " won scoring ", winner.getScore(), " points. ",
                               loser.getName(), " scored ", loser.getScore(), " points."));
   }

   return 0;
}
