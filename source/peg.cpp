#include "format.h"
#include "peg.h"

#include <algorithm>
#include <exception>
#include <map>

unsigned Peg::colorToUint(Peg::Color color)
{
   return unsigned(color);
}

Peg::Color Peg::uintToColor(unsigned index)
{
   if (index < 1 or index > 6)
   {
      throw std::runtime_error{Format::format("Invalid color index ", index, " provided")};
   }

   return Peg::Color(index);
}

std::string Peg::bigPegIndexColorStrInfo()
{
   // clang-format off
   return Format::format(
      colorToUint(Color::red),    "|Red",    "\n",
      colorToUint(Color::green),  "|Green",  "\n",
      colorToUint(Color::blue),   "|Blue",   "\n",
      colorToUint(Color::yellow), "|Yellow", "\n",
      colorToUint(Color::orange), "|Orange", "\n",
      colorToUint(Color::purple), "|Purple"
   );
   // clang-format on
}

std::pair<unsigned /* exact */, unsigned /* colorOnly */> Peg::countMaching(const Peg::Row &a, const Peg::Row &b)
{
   const auto getExactMatches{[](const Peg::Row &a, const Peg::Row &b) {
      auto exact{0u};
      for (auto s{0u}; s < a.size(); s++)
      {
         exact += (a.at(s) == b.at(s));
      }
      return exact;
   }};

   const auto getColorMatches{[](const Peg::Row &a, const Peg::Row &b) {
      std::map<Peg::Color, unsigned /* count */> aColorCounts{};
      std::map<Peg::Color, unsigned /* count */> bColorCounts{};
      std::for_each(a.cbegin(), a.cend(), [&](const Peg::Color &color) { aColorCounts[color]++; });
      std::for_each(b.cbegin(), b.cend(), [&](const Peg::Color &color) { bColorCounts[color]++; });

      auto colorMatching{0u};
      for (const auto &[color, count] : aColorCounts)
      {
         colorMatching += std::min(count, bColorCounts[color]);
      }

      return colorMatching;
   }};

   const auto exact = getExactMatches(a, b);
   const auto colorMatched = getColorMatches(a, b);

   return {exact, colorMatched - exact};
}

Peg::Row Peg::rowFromSet(const std::set<unsigned> &set)
{
   Row row{};

   auto i{0u};
   for (const auto &u : set)
   {
      row[i] = uintToColor(u);
      i++;
   }

   return row;
}
