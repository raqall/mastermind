#include "player.h"

Player::Player(std::string_view name, std::shared_ptr<BoardDataSource> dataSrc)
  : m_name{name}
  , m_score{0u}
  , m_dataSrc{dataSrc}
{
}

Peg::Row Player::getRow(bool allowDuplicates)
{
   return m_dataSrc->getRow(allowDuplicates);
}

std::string_view Player::getName() const
{
   return m_name;
}

unsigned Player::getScore() const
{
   return m_score;
}

void Player::awardPoint()
{
   m_score += 1;
}
