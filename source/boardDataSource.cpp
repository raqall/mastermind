#include "boardDataSource.h"
#include "format.h"

#include <array>
#include <random>
#include <set>

// BoardHumanDataSource

BoardHumanDataSource::BoardHumanDataSource(Cli &cli)
  : m_cli{cli}
{
}

Peg::Row BoardHumanDataSource::getRow(bool allowDuplicates) const
{
   const auto getColorForSlot{[this](Peg::Slot slot) {
      Peg::Color color{};
      auto validInputReceived{false};
      while (!validInputReceived)
         try
         {
            auto input{m_cli.getUnsigned(Format::format("Enter Peg color index at slot ", slot + 1, ": "))};
            if (input >= 1 and input <= 6)
            {
               color = Peg::Color(input);
               validInputReceived = true;
            }
            else
            {
               throw std::runtime_error{Format::format(input, " does not correspond to any color")};
            }
         }
         catch (const std::exception &e)
         {
            m_cli.print(e.what());
         }

      return color;
   }};

   Peg::Row pegs{};
   m_cli.print(Peg::bigPegIndexColorStrInfo());
   for (auto slot{0u}; slot < pegs.size(); slot++)
   {
      pegs[slot] = getColorForSlot(slot);
   }

   return pegs;
}

// BoardCpuDataSource

Peg::Row BoardCpuDataSource::getRow(bool allowDuplicates) const
{
   const auto randUint{[](unsigned from, unsigned to) {
      std::random_device rd;
      std::mt19937 gen(rd());
      std::uniform_int_distribution<unsigned> distrib(from, to);
      return distrib(gen);
   }};

   if (allowDuplicates)
   {
      return Peg::Row{Peg::uintToColor(randUint(1, 6)), Peg::uintToColor(randUint(1, 6)),
                      Peg::uintToColor(randUint(1, 6)), Peg::uintToColor(randUint(1, 6))};
   }
   else
   {
      std::set<unsigned> u{}; // unique
      while (u.size() != 4)
      {
         u.emplace(randUint(1, 6));
      }

      return Peg::rowFromSet(u);
   }
}