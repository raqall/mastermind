#include "board.h"
#include "peg.h"

#include <set>

Board::Board(const Settings &settings, Player &playerOne, Player &playerTwo)
  : m_boardData{settings, playerOne, playerTwo}
{
}

void Board::play()
{
   const auto fillHintRow{[](Peg::Row &hintRow, auto exact, auto colorOnly) {
      auto s{0u};
      while (exact + colorOnly)
      {
         if (exact)
         {
            hintRow.at(s) = Peg::Color::black;
            exact--;
         }
         else if (colorOnly)
         {
            hintRow.at(s) = Peg::Color::white;
            colorOnly--;
         }
         s++;
      }
   }};

   const auto getRowUntilValid{[&](Player &player, bool uniqueOnly) {
      const auto checkUniqueOnly{[](const Peg::Row &row) {
         return row.size() == std::set<Peg::Color>{row.cbegin(), row.cend()}.size();
      }};

      while (true)
      {
         const auto code{player.getRow(uniqueOnly)};
         if (uniqueOnly or checkUniqueOnly(code))
         {
            return code;
         }
         else
         {
            m_boardData.message = "Row contains duplicated colors - try again";
            viewUpdated();
         }
      }
   }};

   auto codeMaker{&(m_boardData.playerTwo)};
   auto codeBreaker{&(m_boardData.playerOne)};
   auto codeBroken{false};
   auto roundsLeft{m_boardData.settings.rounds};
   while (roundsLeft)
   {
      m_boardData.currentRound++;

      std::swap(codeMaker, codeBreaker);

      m_boardData.clear();
      m_boardData.message =
          (m_boardData.currentRound == 1)
              ? Format::format("Starting game. ", codeMaker->getName(), " starts as codemaker. Enter the code.")
              : (codeBroken
                     ? Format::format(codeMaker->getName(),
                                      " you broke the code! New round starts. Now enter new code.")
                     : Format::format("Congratulations ", codeBreaker->getName(), ", your code remained unbroken! ",
                                      codeMaker->getName(), " enter new code."));
      viewUpdated();

      m_boardData.code = getRowUntilValid(*codeMaker, m_boardData.settings.allowDuplicates);
      m_boardData.message = Format::format("Codebreaker ", codeBreaker->getName(), ": enter your attempt");
      viewUpdated();

      for (auto attempt{0u}; attempt < m_boardData.settings.rows; attempt++)
      {
         auto &attemptHint{m_boardData.hints.at(attempt)};
         auto &attemptRow{m_boardData.rows.at(attempt)};

         m_boardData.message = Format::format("Keep trying ", codeBreaker->getName());
         attemptRow = getRowUntilValid(*codeBreaker, m_boardData.settings.allowDuplicates);

         auto [exact, colorOnly]{Peg::countMaching(m_boardData.code, attemptRow)};
         codeBroken = (exact == m_boardData.settings.rowSlots);

         fillHintRow(attemptHint, exact, colorOnly);

         codeMaker->awardPoint();
         if (attempt == m_boardData.settings.rows) // award extra point if the codebreaker used all their chances
         {
            codeMaker->awardPoint();
         }

         viewUpdated();

         if (codeBroken)
         {
            break;
         }
      }

      m_boardData.clear();
      m_boardData.message.clear();
      viewUpdated();

      roundsLeft--;
   }
}

void Board::setViewUpdatedListener(const std::function<void(const BoardData &)> &fun)
{
   m_onBoardUpdateHdlr = fun;
}

void Board::viewUpdated() const
{
   if (m_onBoardUpdateHdlr)
   {
      m_onBoardUpdateHdlr(m_boardData);
   }
}