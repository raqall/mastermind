# Mastermind
### How to run?

```sh
git clone https://gitlab.com/raqall/mastermind.git
cd mastermind
make build
make run
```

Tested on macOS 12 (Apple clang version 13.0.0).